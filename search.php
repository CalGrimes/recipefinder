<?php
include("api/master.php");
session_start();

$the_page = new HTMLPage("RecipeFinder");
$content = "";



$content = <<<HTML
		<div id="#searchBar">
			<div class="wrapper">
                <form id="searchBar" action="searchItems.php" method="post">
				   <input id="search" type="search" name="searchItem" class="inputSearch"
					  placeholder="Search Recipes">
				   <div class="searchIcon">
				       <input id="submit" type="submit" value="Search"></input>
				   </div>
                </form>
			</div>
		</div>
        <article id="searchResults">
        </article>
        <script>
        var frm = $('#searchBar');
        frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
                $('#searchResults').html(data); 
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });
        </script>
HTML;








require ('api/login.php');
$content;

$the_page->setBody($content);
$the_page->renderPage();
?>