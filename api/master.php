<?php

class HTMLPage
{

    // ----CLASS MEMBERS-------------------------
    private $_week_num = 1;

    private $_head_title = "Recipe Finder";

    // Store the Page Title
    private $_head_html = "";

    // Store any other HTML in the Head
    private $_nav_bar = "";

    private $_footer = "";

    private $_body_content = "";

    // Store the Body Content
    private $_page_content = "";

    // -----CONSTRUCTORS-------------------------
    function __construct($page_title)
    {
        $this->setPageTitle($page_title);
        $this->setHead();
        $this->setNavBar();
        $this->setFooter();
    }

    // -----SETTERS---------------
    public function setPageTitle($ptitle)
    {
        $this->_head_title = $ptitle;
    }

    public function setHead()
    {
        $this->_head_html = <<<HEAD
        <head>
        <!--  SET THE CHARACTER SET -->
        <meta charset="UTF-8">
        <!--  SET THE PAGE TITLE (SHOWN IN TAB ON CHROME)-->
        <title>$this->_head_title</title>
        <!--  SETUP SOME META DATA FOR THIS PAGE -->
        <meta name="keywords" content="HTML,CSS,5130COMP">
        <meta name="author" content="863040">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--  INCLUDE OUR CSS STYLE SHEETS -->
        <link rel="stylesheet" type="text/css" href="css/stylistic.css">
        <!--Latest compiled and minified CSS -->
        <link rel="stylesheet"
        	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--Optional theme -->
        <link rel="stylesheet"
        	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <!--Latest compiled and minified JavaScript -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script
        	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </head>
        HEAD;
    }

    public function setNavBar()
    {
        if(isset($_SESSION['user']) & !empty($_SESSION['user'])){
            $userhtml = <<<NAV
                    <form class="nav navbar-nav navbar-right" action="index.php" method="post">
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Logged in as: {$_SESSION['user']}<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="accProfile.php">Account</a></li>
                                <li><input id="logout" name="logout" type="submit" value="Logout"></li>
                            </ul>
                        </li>
                    </form>
               
            NAV;
        }
        else {
            $userhtml = <<<NAV
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="main.php">Account</a></li>
            </ul>
            NAV;
        }
        $this->_nav_bar = <<<NAV
        <div id="wrapper">
            <nav class="navbar navbar-inverse">
            			<div class="container-fluid">
            				<div class="navbar-header">
            					<button type="button" class="navbar-toggle collapsed"
            						data-toggle="collapse" data-target="#main-menu"
            						aria-expanded="false">
            						<span class="sr-only">Toggle navigation</span> <span
            							class="icon-bar"></span> <span class="icon-bar"></span> <span
            							class="icon-bar"></span>
            					</button>
            					<a class="navbar-brand" href="#">RecipeFinder</a>
            				</div>
            				<div id="main-menu" class="collapse navbar-collapse">
            					<ul class="nav navbar-nav">
            						<li class="active"><a href="main.php">Home</a></li>
            					</ul>
            					{$userhtml}
            					<ul class="nav navbar-nav">
            						<li class="dropdown"><a class="dropdown-toggle"
            							data-toggle="dropdown" href="Recipes.php">Recipes <span
            								class="caret"></span></a>
            							<ul class="dropdown-menu ">
            								<li><a href="recipes.php">All Recipes</a></li>
            								<li><a href="#">Featured Recipes</a></li>
            							</ul></li>
            					</ul>
                                <ul class="nav navbar-nav">
                                    <li><a href="search.php">Search</a></li>
                                </ul>
            				</div>
            			</div>
            	</nav>
           </div>
        NAV;
    }

    public function setFooter()
    {
        $this->_footer = <<<FOOT
            <footer>
                <div id="columns">
                    <div id="colleft">
                        <b>Accessibility Help</b><br />
                        <a href="#">Index</a><br />
                        <a href="#">Change Theme</a>
                    </div>
                    <div id="colright" class="pull-right">
                        <b>Customer Servies</b><br />
                        <a href="#">FAQ</a><br />
                        <a href="#">Contact Information</a>
                    </div>
                </div>
            </footer>
        FOOT;
    }

    public function setBody($pbodycontent)
    {
        $this->_body_content = $pbodycontent;
    }

    // -----PUBLIC FUNCTIONS----------------------
    public function renderPage()
    {
        echo $this->createPage();
    }

    public function createPage()
    {
        $thtmlmarkup = <<<HTML
        <!DOCTYPE html>
        <html lang="en">
        <!--HEAD ELEMENT -->
        $this->_head_html
        <!--BODY ELEMENT -->
        <body>
        <div class="container">
        $this->_nav_bar
        $this->_body_content
        $this->_footer
        </div>
        </body>
        </html>
        HTML;
        return $thtmlmarkup;
    }
}
?>