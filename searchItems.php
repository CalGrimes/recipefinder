<?php
$searchItem = $_POST['searchItem'];

require ('api/login.php');



$resultsHTML = "<h4>Search Results:</h4> <br />";

while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $resultsHTML .= renderResults($row);
}

$resultsHTML .= <<<HTML
    <div class="row">
    </div>
HTML;

echo $resultsHTML;


function renderResults($row){
    $content = <<<HTML
        <div class="col-md-6 searchResult">
        <img class="searchItem" src="img/{$row['recipeImg']}.jpg"><br />
        {$row['recipeName']}
        <form method="post" action="individualRecipe.php?id={$row['recipeID']}">
            <button type="submit">View</button>
        </form>
        </div>
        
HTML;
    return $content;
}
?>