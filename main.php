<?php
session_start();

include("api/master.php");


$the_page = new HTMLPage("RecipeFinder");

$content = "";
function make_loggedOutPage() {

    $content = <<<HTML
<div id="main">
    <h1 id="center">Welcome</h1>
        <h3>Login</h3>
        <form id="frmLogin" action="api/app_entry.php" method="POST">
            <div class="form-group">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
            </div>
            <div class="form-group">
                <label for="pin"><b>PIN</b></label>
                <input type="text" name="pin" placeholder="Enter PIN" name="pin" pattern="[0-9]{4}" minlength="4" maxlength="4" oninvalid="setCustomValidity('Please enter 4 digits')">
            </div>
            <input type="submit" name="submit" value="Login">
        </form>
    
        <h2>OR</h2>
        <h3>Register</h3>
        <form id="frmReg" action="api/app_entry.php" method="POST">
            <div class="form-group">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" required>
            </div>
            <div class="form-group">
                <label for="email"><b>eMail Address</b></label>
                <input type="text" placeholder="Enter eMail Address" name="email" required>
            </div>
            <div class="form-group">
                <label for="pin"><b>PIN</b></label>
                <input type="text" name="pin" placeholder="Enter PIN" pattern="[0-9]{4}" minlength="4" maxlength="4" oninvalid="setCustomValidity('Please enter 4 digits')">
            </div>
            <div class="form-group">
            <label>Chef?</label>
                <select name="chefOp">
                    <option value="no">No</option>
                    <option value="yes">Yes</option>
                </select>
            </div>
            <input  type="submit" name="submit" value="Register">
        </form>     
</div>

HTML;
        return $content;
}


function make_homePage($conn)
{
    //Get 3 recipes to feature
    $imgName = "";
    $recipeName = "";

    //get number of rows
    $query = "SELECT recipeID AS recipeID FROM recipes";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $ids_array[] = $row['recipeID'];
    }

    shuffle($ids_array);

    $query = "SELECT recipeID, recipeDetails, recipeImg, recipeName FROM recipes WHERE recipeID = $ids_array[0] OR  recipeID = $ids_array[1] OR  recipeID = $ids_array[2]";
    $result = $conn->query($query);
    if (!$result) die($conn->error);

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $featured_array[] = $row;

    }

    $content = <<<HTML
<div id="main">
    <h1>Featured Recipes</h1>
    <article>
        <section class="col-md-4 col-xs-6">
                <img class="featuredImg" src="img/{$featured_array[0]['recipeImg']}.jpg"
                style="max-width: 100%; max-height: 100%;">
                <b>{$featured_array[0]['recipeName']}</b> {$featured_array[0]['recipeDetails']}
                <form method="post" action="individualRecipe.php?id={$featured_array[0]['recipeID']}">
                    <button type="submit">View</button>
                </form>
        </section>
        <section class="col-md-4 col-xs-6">
                <img class="featuredImg" src="img/{$featured_array[1]['recipeImg']}.jpg"
                style="max-width: 100%; max-height: 100%;">
                <b>{$featured_array[1]['recipeName']}</b> {$featured_array[1]['recipeDetails']}
                <form method="post" action="individualRecipe.php?id={$featured_array[1]['recipeID']}">
                    <button type="submit">View</button>
                </form>
        </section>
        <div class="row">
    
        <section class="col-md-4 col-xs-6">
                <img class="featuredImg" src="img/{$featured_array[2]['recipeImg']}.jpg"
                style="max-width: 100%; max-height: 100%;">
                <b>{$featured_array[2]['recipeName']}</b> {$featured_array[2]['recipeDetails']}
                <form method="post" action="individualRecipe.php?id={$featured_array[2]['recipeID']}">
                    <button type="submit">View</button>
                </form>
        </section>
        </div>
    </article>
        
    <div class="row">
    </div>


</div>
HTML;


    return $content;
}



require("api/login.php");


if(empty($_SESSION['user'])) {
    $content = make_loggedOutPage();
}
else {
    $the_page ->setNavBar();
    $content = make_homePage($conn);
}

$the_page->setBody($content);
$the_page->renderPage();

?>