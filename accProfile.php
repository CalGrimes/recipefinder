<?php
include("api/master.php");
session_start();

$the_page = new HTMLPage("RecipeFinder");
$content = "";


function make_profilePage($conn) {

    //get accountID and email

    $query = "SELECT username, pin, accountID, email, userType FROM account HAVING username = '{$_SESSION["user"]}'";

    $result = $conn->query($query);

    if (!$result) die($conn->error);


    //ID to check favourites, email as profile info

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {

        $rows[] = array("accountID" => $row['accountID'],
            "email" => $row['email'], "pin" => $row['pin']);
    }

    //get the recipeIDs in users favourites
    $query = "SELECT recipeID FROM favourites WHERE accountID = {$rows[0]['accountID']}";
    $result = $conn->query($query);
    if (!$result) die($conn->error);

    //search for all the recipes by their recipeID
//     $count = 0;

    $rowhtml = "";
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $query = "SELECT recipeID, recipeImg, recipeName FROM recipes WHERE recipeID = '{$row['recipeID']}'";
        $resultFav = $conn->query($query);
        if (!$result) die($conn->error);

        while($row1 = mysqli_fetch_array($resultFav, MYSQLI_ASSOC)) {
            $rowhtml .= renderFavRow($row1['recipeID'], $row1['recipeImg'], $row1['recipeName']);
        }
    }




    //go thru favRows and add row to renderFavRow
//     foreach($rowsFavAll as $x) {
//         $rowhtml .= renderFavRow($x);
//     }
//     while($row = mysqli_fetch_array($rowsFav, MYSQLI_ASSOC)) {
//         echo $row['recipeName'];
//         $rowhtml .= renderFavRow($row);
//     }

    //generate the page
    if (empty($rowhtml)) {
        $rowhtml = "No Favourite Recipes";
    }
    $content = <<<HTML
        <div class="pDetails">
            <h1>Hello {$_SESSION["user"]}</h1>
            <b>
            Your Details:
            </b>
            <br />
            Your Username is: {$_SESSION["user"]}
            <br />
            Your Pin is: {$rows[0]['pin']}
            <br />
            Your email is: {$rows[0]['email']}
        </div>
        <div class="favourites">
            <h2>Your Favourite Recipes</h2>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Recipe Image</th>
                        <th>Recipe Name</th>
                    </tr>
                </thead>
            <tbody>
                {$rowhtml}
            </tbody>
        </div>
    HTML;

                return $content;



}
function renderFavRow($recipeID, $recipeImg, $recipeName) {
    $rowhtml = <<<FROW
                <tr>
        		    <td>
                        <img class="favImg" src="img/{$recipeImg}.jpg">
                        <form method="post" action="individualRecipe.php?id={$recipeID}">
                            <button type="submit">View</button>
                        </form>
                    </td>
                    <td>{$recipeName}</td>
                </tr>
    FROW;


    return $rowhtml;
}


require ('api/login.php');
$content = make_profilePage($conn);

$the_page->setBody($content);
$the_page->renderPage();
?>