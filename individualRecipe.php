<?php
include("api/master.php");
session_start();

$the_page = new HTMLPage("RecipeFinder");
$content = "";

function make_recipePage($conn) {

    $query = "SELECT recipeImg, recipeDetails, recipeName, categories, ingredients, steps FROM recipes WHERE recipeID = {$_REQUEST['id']}";
    $result = $conn->query($query);
    if (!$result) die($conn->error);



    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $recipeRow[] = array("recipeImg" => $row['recipeImg'], "recipeDetails" => $row['recipeDetails'],"recipeName" => $row['recipeName'], "categories" => $row['categories'], "ingredients" => $row['ingredients'], "steps" => $row['steps']);
    }


    $buttonHTML = "";
    if ($_SESSION['userType'] == "chef" || $_SESSION['userType'] == "admin") {
        $buttonHTML = <<<HTML
            <form method="post" action="">
                <button type="submit" name="edit">Edit Recipe</button>
                <button type="submit" name="delete">Delete Recipe</button>
            </form>
        HTML;
    }
    $ingredArr = explode(".", $recipeRow[0]['ingredients']);
    $stepArr = explode("\\", $recipeRow[0]['steps']);

    //go thru array
    //output each

    $ingredHTML = "";
    foreach ($ingredArr as $x) {
        $x .= "<br />";
        $ingredHTML .= $x;
    }

    $stepsHTML = "";
    foreach ($stepArr as $x) {
        $x .= "<br />";
        $stepsHTML .= $x;
    }


    //get all the ratings for the recipes
    $avRatings = getOverallRating($conn);


    if($avRatings == null) {
        $avRatings[0]['avComplexity'] = "No Rating";
        $avRatings[0]['avAesthetics'] = "No Rating";
        $avRatings[0]['avTaste'] = "No Rating";
    }

    //get reviews of recipe
    $query = "SELECT complexity, aesthetics, taste, reviewDesc, accountID FROM reviews WHERE recipeID = {$_REQUEST['id']}";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    $revCount = 0;

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $reviews[$revCount] = array("complexity" => $row['complexity'], "aesthetics" => $row['aesthetics'],"taste" => $row['taste'], "reviewDesc" => $row['reviewDesc'], "accountID" => $row['accountID']);
    }

    $reviewsHTML = renderReview($reviews, $conn);

    $content = <<<HTML
        <h1>Recipe Details</h1>
        <h4><b>{$recipeRow[0]['recipeName']}</b></h4>

        <div class="row">
            <div class="col-md-6">
            <img src="img/{$recipeRow[0]['recipeImg']}.jpg"
                style="max-width: 100%; max-height: 100%;">
            </div>
            <div class="col-md-6">
            {$recipeRow[0]['recipeDetails']}<br />
            {$buttonHTML}<br />
            Complexity: {$avRatings[0]['avComplexity']}<br />
            Aesthetics: {$avRatings[0]['avAesthetics']}<br />
            Taste: {$avRatings[0]['avTaste']}<br />

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <b>Ingredients:</b><br />
                    {$ingredHTML}
                    <b>Steps:</b><br />
                    {$stepsHTML}
            </div>
            <div class="col-md-6">
            <h4>Reviews</h4>
            {$reviewsHTML}
            </div>
        </div>



HTML;




    return $content;
}

function getOverallRating($conn){


    $query = "SELECT complexity, aesthetics, taste FROM reviews WHERE recipeID = {$_REQUEST['id']}";
    $result = $conn->query($query);
    if (!$result) die($conn->error);

    $totalComplexity = 0;
    $totalAesthetics = 0;
    $totalTaste = 0;
    $count = 0;

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $totalComplexity += $row['complexity'];
        $totalAesthetics += $row['aesthetics'];
        $totalTaste += $row['taste'];
        $count = $count + 1;
    }
    if($totalComplexity != 0) {
        $avRatings[] = array("avComplexity" => $totalComplexity / $count, "avAesthetics" => $totalAesthetics / $count, "avTaste" => $totalTaste / $count);
        return $avRatings;
    }
    return null;

}

function renderReview($reviews, $conn) {
    $content = "";
    foreach ($reviews as $x) {
        $query = "SELECT username FROM account WHERE accountID = {$x['accountID']}";
        $result = $conn->query($query);
        if (!$result) die($conn->error);
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $account = array("username" => $row['username']);
        }

        $content .= <<<HTML
        Reviewed By: {$account['username']}<br />
        Complexity: {$x['complexity']}<br />
        Aesthetics: {$x['aesthetics']}<br />
        Taste: {$x['taste']}<br />
        <br />
        Description: {$x['reviewDesc']}
        <br />

        
HTML;
        return $content;
    }
}



require ('api/login.php');
$content = make_recipePage($conn);

$the_page->setBody($content);
$the_page->renderPage();
?>