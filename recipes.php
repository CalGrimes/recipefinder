<?php
include("api/master.php");
session_start();

$the_page = new HTMLPage("RecipeFinder");
$content = "";

$_SESSION['page'] = 1;


//Table generates on document ready all recipes
//Another script on click (categorie type) generate table sorted
$content = <<<HTML

<form id="sort" action="recipeFetch.php" method="post">
    <h4>Sort By</h4>
        <select>
            <option>--</option>
            <option>Lunch</option>
            <option>Dinner</option>
            <option>Snack</option>
        </select>
    <input id="sortBtn" type="submit" name="submit">
</form>
<div id="displayRecipes">
</div>
<script>
$(function(){
    $.get( "recipeFetch.php", function( data ) {
        $( "#displayRecipes" ).html( data );
            alert( "All Recipes Loaded." );
    });
});
var frm = $('#sort');
$( "#sortBtn" ).click(function() {
    $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
                $('#displayRecipes').html(data); 
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
});
</script>



HTML;




//Display buttons to sort by categories, etc





// $content = make_profilePage($conn);

$the_page->setBody($content);
$the_page->renderPage();
?>