<?php
require ('api/login.php');
session_start();

if ($_SESSION['page'] == 1) {
    $_SESSION['page'] = 0;
    $query = "SELECT categories, recipeImg, recipeName FROM recipes";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    $allRecipesHtml = "";

    $allRecipesHtml = <<<HTML
    <h4>All Recipes</h4>
    <table>
    <tr>
        <th>Image</th>
        <th>Name</th>
        <th>Categories</th>
    </tr>
    HTML;

    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $allRecipesHtml .= <<<HTML
    <tr>
        <td><img class="recipes" src="img/{$row['recipeImg']}.jpg"></td>
        <td>{$row['recipeName']}</td>
        <td>{$row['categories']}</td>
    </tr>
    HTML;
    }

    $allRecipesHtml .= "</table>";
    echo $allRecipesHtml;


}
else {
    $sortType = $_POST['submit'];
    echo "<h1>Incomplete</h1>";
}